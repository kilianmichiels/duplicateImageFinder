# Duplicate Image Finder

Search for duplicate files inside a given directory.  
Next, choose which files get to stay and which are removed.    
**Support for all the PILLOW supported formats ([more information](https://pillow.readthedocs.io/en/5.2.x/handbook/image-file-formats.html#fully-supported-formats))**

![example](example.gif)

## Usage
This application is created for Python3.x.

- Make sure you have the required dependencies installed. Install them by running: `[sudo] pip install -r requirements.txt` in your shell.
- Run the script as follows: `python main.py - -l LOCATION -f FILETYPE` (Attention: the first hyphen is necessary for an issue between argparse and kivy)
- The program will look for duplicates in the given location and all its subdirectories.
- Choose which files you want to **REMOVE** and click _Next_.
- When all the duplicates are processed, the window will close and a final prompt will ask you if you are certain you want to delete the files.
- Type `y` for Yes and `n` for No.

**!!! Warning !!!**  
Agreeing to removing the files in the program will permanently delete these files from your computer. Only agree when you are certain. 

## Options
Running the program with the following optional arguments is possible:  

- `-v` or `--verbose`: Show more information about what is happening.
- `-d` or `--debug`: Debugging information for developers.
- `-c` or `--confidence`: Give a confidence percentage of how similar images should be. This should be between 0 and 100. Best results are expected for a confidence percentage of 90 and higher.

## Help
For more help, simply type python `main.py - -h` in your terminal.

## Known Issues
- Running the program on Windows can give some difficulties with Kivy dependencies. Visit the [Kivy installation manual](https://kivy.org/docs/installation/installation-windows.html) if there are any problems.

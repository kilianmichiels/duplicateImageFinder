# MIT License
#
# Copyright (c) 2018 Michiels Kilian
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import glob2
from inspect import currentframe, getframeinfo
import os
from tqdm import tqdm


class FileOrganizer:
    def __init__(self, root_location, extension, communicator, backuphandler):
        self.root_location = root_location
        self.extension = extension
        self.communicator = communicator
        self.backuphandler = backuphandler
        self.communicator.inform_debug('FileOrganizer created',
                                       getframeinfo(currentframe()))

    def find(self):
        """
        Search for the files.
        :return:
        """
        self.communicator.inform_verbose(
            'Looking for {} files in {}'.format(
                self.extension, self.root_location))
        return glob2.glob(self.root_location + '/**/*.' + self.extension)

    def request_permission_to_delete(self):
        """
        Give the user a final warning to make sure these files may be deleted.
        :return: True if files are deleted. False otherwise.
        """
        self.communicator.inform_warning(
            'These files will be permanently deleted from your computer.')
        confirmation = self.communicator.request_input(
            'Are you certain you want to delete the selected '
            'files? (y/N) ', ['y', 'Y', 'N', 'n'])
        if confirmation == 'y' or confirmation == 'Y':
            self.communicator.inform_verbose('Deleting files...')
            return True
        elif confirmation == 'n' or confirmation == 'N':
            self.communicator.inform_verbose('Files will not be deleted.')
            return False

    def delete(self, files):
        success = True
        failed_files = []
        if self.request_permission_to_delete():
            for f in tqdm(files, ascii=True, dynamic_ncols=True,
                          leave=True):
                try:
                    os.remove(f)
                except FileNotFoundError:
                    success = False
                    failed_files.append(f)
            return success, failed_files
        else:
            return False, None

# MIT License
#
# Copyright (c) 2018 Michiels Kilian
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# The Communicator handles all the communication between the user and the
# program.

import os
import sys
from inspect import currentframe, getframeinfo

from tqdm import tqdm


class Communicator:
    BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE = range(8)
    ascii_name = (' ___            _ _         _       ___                     '
                  '___ _         _\n|   \ _  _ _ __| (_)__ __ _| |_ ___|_ _|_ _'
                  '_  __ _ __ _ ___| __(_)_ _  __| |___ _ _\n| |) | || | \'_ \ '
                  '| / _/ _` |  _/ -_)| || \'  \/ _` / _` / -_) _|| | \' \/ _` '
                  '/ -_) \'_|\n|___/ \_,_| .__/_|_\__\__,_|\__\___|___|_|_|_\__'
                  ',_\__, \___|_| |_|_||_\__,_\___|_|  \n          |_|         '
                  '                            |___/\n')

    def __init__(self, verbose, debug):
        # Introduce yourself
        os.system('cls' if os.name == 'nt' else 'clear')
        # Check verbosity. The higher the level, the more output.
        self.verbosity_level = 0
        if verbose:
            self.verbosity_level += 1
        if debug:
            self.verbosity_level += 2

        # Check if the terminal can handle colours
        self.has_colours = self.has_colours(sys.stdout)
        tqdm.write(self.colorize_text(self.ascii_name, self.CYAN))
        tqdm.write('')
        self.inform_verbose('Verbosity turned ON. You will see a more '
                            'detailed program flow.')
        self.inform_debug('Debugging information will be displayed.',
                          getframeinfo(currentframe()))

        self.inform_debug('Communicator created', getframeinfo(currentframe()))

    def inform_verbose(self, text):
        if self.verbosity_level > 0:
            tqdm.write('''
{}{}
{}
        '''.format(self.colorize_text('[INFO] '),
                   self.colorize_text(text),
                   self.colorize_text('_' * (7 + len(text)))))
        else:
            tqdm.write(".")

    def inform_success(self, text):
        tqdm.write('''
{}{}
{}
        '''.format(self.colorize_text('[SUCCESS] ', self.GREEN),
                   self.colorize_text(text, self.GREEN),
                   self.colorize_text('_' * (10 + len(text)), self.GREEN)))

    def inform_error(self, text):
        tqdm.write('''
{}{}
{}
        '''.format(self.colorize_text('[ERROR] ', self.RED),
                   self.colorize_text(text, self.RED),
                   self.colorize_text('_' * (7 + len(text)), self.RED)))

    def inform_warning(self, text):
        tqdm.write('''
{}{}
{}
        '''.format(self.colorize_text('[WARNING] ', self.YELLOW),
                   self.colorize_text(text, self.YELLOW),
                   self.colorize_text('_' * (9 + len(text)), self.YELLOW)))

    def inform_debug(self, text, frameinfo):
        if self.verbosity_level > 1:
            tqdm.write('''
{}{}\n{}{}{}{}
        '''.format(self.colorize_text('[DEBUG] ', self.MAGENTA),
                   self.colorize_text(text, self.MAGENTA),
                   self.colorize_text('--> In ', self.MAGENTA),
                   self.colorize_text(frameinfo.filename, self.MAGENTA),
                   self.colorize_text(' on line ', self.MAGENTA),
                   self.colorize_text(str(frameinfo.lineno), self.MAGENTA)))

    def inform_list(self, list_to_print):
        if self.verbosity_level > 0:
            for f in list_to_print:
                tqdm.write('''{}{}'''.format(self.colorize_text('--> '),
                                             self.colorize_text(f)))

    def request_input(self, question, possibilities):
        answer = input('{}{}'.format(self.colorize_text('[QUESTION] '),
                                     self.colorize_text(question)))
        if answer in possibilities:
            return answer
        else:
            self.inform_error(
                'Please answer the question with one of the possible answers.')
            self.request_input(question, possibilities)

    @staticmethod
    def has_colours(stream):
        if not hasattr(stream, "isatty"):
            return False
        if not stream.isatty():
            return False
        try:
            import curses
            curses.setupterm()
            return curses.tigetnum("colors") > 2
        except ImportError:
            return False

    def colorize_text(self, text, colour=WHITE):
        if self.has_colours:
            return "\x1b[1;%dm" % (30 + colour) + text + "\x1b[0m"
        else:
            return text

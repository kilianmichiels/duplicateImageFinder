# MIT License
#
# Copyright (c) 2018 Michiels Kilian
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


from inspect import currentframe, getframeinfo

from kivy.app import App
from kivy.core.window import Window
from kivy.graphics import Color, Rectangle
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.checkbox import CheckBox
from kivy.uix.gridlayout import GridLayout
from kivy.uix.image import Image
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.progressbar import ProgressBar


# TODO: Ugly, fix this so it is not global.
current_images = []
current_widgets = []
checkboxes = []
current_labels = []


class RootWidget(BoxLayout):
    pass


class CustomImageBox(BoxLayout):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        with self.canvas:
            Color(1, 1, 1, 1)
            self.rect = Rectangle(pos=self.pos, size=self.size)

        self.bind(pos=self.update_rect)
        self.bind(size=self.update_rect)

    def update_rect(self, *args):
        self.rect.pos = self.pos
        self.rect.size = self.size


class ProgressBox(BoxLayout):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        with self.canvas:
            Color(0, 0, 0, 1)
            self.rect = Rectangle(pos=self.pos, size=self.size)

        self.bind(pos=self.update_rect)
        self.bind(size=self.update_rect)

    def update_rect(self, *args):
        self.rect.pos = self.pos
        self.rect.size = self.size


class LocationDisplay(BoxLayout):

    def __init__(self, text_to_display, **kwargs):
        super(LocationDisplay, self).__init__(**kwargs)
        self.text_to_display = text_to_display

        self.orientation = "vertical"

        self.my_output = Label(text='[ref=' + self.text_to_display + ']' +
                                    self.text_to_display + '[/ref]',
                               text_size=(None, None),
                               color=[0, 0, 0, 1], size_hint=(.8, 1),
                               size=self.size,
                               height=self.size[1],
                               halign="center",
                               valign="middle",
                               markup=True, )

        self.my_output.bind(on_ref_press=self.label_pressed)

        self.add_widget(self.my_output)

        """BINDING THE LABEL TO THE FUNCTION THAT UPDATES THE SIZE"""
        self.my_output.bind(size=self.setting_function)

    def setting_function(self, *args):
        self.my_output.pos_hint = {'center_x': 0.5, 'center_y': .85}
        self.my_output.text_size = self.size

    def label_pressed(self, instance, value):
        for i, x in enumerate(current_images):
            if x == value:
                if checkboxes[i].active:
                    checkboxes[i].active = False
                else:
                    checkboxes[i].active = True


class CustomGridLayout(GridLayout):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        with self.canvas:
            Color(0, 0, 0, 1)
            self.rect = Rectangle(pos=self.pos, size=self.size)

        self.bind(pos=self.update_rect)
        self.bind(size=self.update_rect)

    def update_rect(self, *args):
        self.rect.pos = self.pos
        self.rect.size = self.size


class CustomNextButton(Button):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.background_color = [.5, .9, .45, 1]
        self.background_normal = ''
        self.font_size = 45


class CustomCheckBox(CheckBox):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.color = [.5, 1, .45, 1]


class Cleaner(App):
    title = 'Photo Finder'

    def __init__(self, files, communicator, **kwargs):
        super().__init__(**kwargs)
        self.image_grid = CustomGridLayout(cols=3)
        self.files = files
        self.communicator = communicator

        self.counter = 0
        self.images_to_delete = set()
        self.keys = list(self.files.keys())

        self.root = RootWidget(orientation="vertical")
        self.pb = ProgressBar(max=len(self.keys), value=1, size_hint=(.8, 1))
        self.pb_label = Label(text='{}/{}'.format(int(self.pb.value),
                                                  len(self.keys)),
                              size_hint=(.2, 1))
        self.popup = None

        self.communicator.inform_debug('Cleaner created',
                                       getframeinfo(currentframe()))

    def build(self):
        # Show popup
        explanation = (
            "Hello! You get to choose which photos you want to REMOVE."
            "\nPlease select these by checking the checkbox and press next. "
            "The images will be deleted AT THE END \nof the program."
            "If you made a mistake, simply quit the application and your images"
            "will not be deleted! \n\nGood luck!")

        popup_content = GridLayout(cols=1)
        popup_content.add_widget(Label(text=explanation, size_hint=(1, .8)))
        popup_button = Button(text='OK!', size_hint=(1, .2))
        popup_content.add_widget(popup_button)
        self.popup = Popup(title="Explanation", content=popup_content,
                           size=(400, 400), auto_dismiss=False)
        popup_button.bind(on_press=self.continue_after_popup)
        self.popup.open()

    def on_stop(self):
        return True

    def continue_after_popup(self, args):
        self.popup.dismiss()

        self.refresh()

        # Add images
        self.root.add_widget(self.image_grid)

        # Add next button
        self.root.add_widget(
            CustomNextButton(text="Next", on_press=self.next_pressed,
                             size_hint=(1, .09)))

        # Add progress bar with counter.
        self.root.add_widget(self.create_progress_bar())

    def add_new_image(self, image_location):
        cib = CustomImageBox(spacing=5, orientation='vertical',
                             size_hint=(1, .8))
        ctb = CustomImageBox(padding=5, spacing=5, orientation='horizontal',
                             size_hint=(1, .2))
        image = Image(source=image_location, size_hint=(1, .8))
        label = LocationDisplay(text_to_display=image_location)
        checkbox = CustomCheckBox(size_hint=(.2, 1))

        current_images.append(image_location)
        checkboxes.append(checkbox)
        current_labels.append(label)

        ctb.add_widget(label)
        ctb.add_widget(checkbox)
        cib.add_widget(image)
        cib.add_widget(ctb)

        return cib

    def next_pressed(self, args):
        self.communicator.inform_debug('Checking checkboxes',
                                       getframeinfo(currentframe()))
        # Fetch the selected images and add them to the images to delete
        for i, x in enumerate(checkboxes):
            if x.active:
                self.images_to_delete.add(current_images[i])

        self.communicator.inform_debug('Updating progress bar',
                                       getframeinfo(currentframe()))
        # Update progress bar
        self.update_progress_bar()

        self.communicator.inform_debug('Cleaning display',
                                       getframeinfo(currentframe()))
        # Clean display
        for x in current_widgets:
            self.image_grid.remove_widget(x)

        self.communicator.inform_debug('Updating counter',
                                       getframeinfo(currentframe()))
        # Update with new images...
        self.counter += 1

        self.communicator.inform_debug(
            'Checking if there are any duplicates left',
            getframeinfo(currentframe()))
        # If there are any
        if self.counter < len(self.keys):
            self.communicator.inform_debug('Refreshing',
                                           getframeinfo(currentframe()))
            self.communicator.inform_debug('---------------------',
                                           getframeinfo(currentframe()))
            self.refresh()
        # If there are no more images left
        else:
            self.communicator.inform_verbose(
                'No more duplicates to evaluate. Exiting GUI.')
            Window.hide()
            App.get_running_app().stop()

    def update_progress_bar(self):
        self.pb.value += 1
        self.pb_label.text = '{}/{}'.format(int(self.pb.value),
                                            len(self.keys))

    def clean(self):
        self.run()
        return self.images_to_delete

    def refresh(self):
        # Clean lists
        self.clean_lists()

        g = self.add_new_image(self.keys[self.counter])
        self.image_grid.add_widget(g)
        current_widgets.append(g)

        for i, k in enumerate(self.files[self.keys[self.counter]]):
            g = self.add_new_image(k)
            self.image_grid.add_widget(g)
            current_widgets.append(g)

        self.communicator.inform_debug('Key: ' + str(self.keys[self.counter]),
                                       getframeinfo(currentframe()))
        self.communicator.inform_debug('Values: ' +
                                       str(self.files[self.keys[self.counter]]),
                                       getframeinfo(currentframe()))

    def clean_lists(self):
        global current_images, current_widgets, current_labels, checkboxes
        current_images = []
        current_widgets = []
        current_labels = []
        checkboxes = []

    def create_progress_bar(self):
        cpb = ProgressBox(padding=10, orientation='horizontal',
                          size_hint=(1, .05))
        cpb.add_widget(self.pb)
        cpb.add_widget(self.pb_label)
        return cpb

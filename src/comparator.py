# MIT License
#
# Copyright (c) 2018 Michiels Kilian
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
from inspect import currentframe, getframeinfo
import numpy as np
from PIL import Image
from tqdm import tqdm
from time import time, strftime, gmtime


class Comparator:
    def __init__(self, files, communicator, backuphandler,
                 processed_duplicates=None, processed_files=None):
        self.files = files
        self.communicator = communicator
        self.backuphandler = backuphandler
        self.service_time = time()

        if processed_duplicates is not None:
            self.communicator.inform_debug('Backup file used:',
                                           getframeinfo(currentframe()))

            self.duplicates = processed_duplicates
            if self.duplicates is not None:
                self.communicator.inform_debug('--> Duplicates found',
                                               getframeinfo(currentframe()))
            self.processed_files = processed_files
            if self.processed_files is not None:
                self.communicator.inform_debug('--> Processed files found',
                                               getframeinfo(currentframe()))
        else:
            self.communicator.inform_debug('Backup file not used',
                                           getframeinfo(currentframe()))
            self.duplicates = {}
            self.processed_files = []

        self.communicator.inform_debug('Comparator created',
                                       getframeinfo(currentframe()))

    ############################################################################
    #                                 COMPARE PHOTOS                           #
    ############################################################################
    def compare_photos(self):
        """
        Compare the given images and mark duplicates.

        Algorithm:
        --> Fetch the file size
        --> Image comparing to others = p1, Other images = p2
        --> If p1 and p2 not the same, not yet processed and
            not yet in the duplicate set,
            I) Compare the file size
            II) Compare dimensions
            III) Compare the generated hash of both images
                 This should be equal
        --> If these checks are all passed, the image is seen as duplicate of p1

        Backup's are written every 10 processed p1 images.

        :return: All images marked as duplicates
        """
        self.communicator.inform_verbose('Fetching files...')

        # Fetch the size of the files once
        p_file_sizes = self.get_all_file_sizes()

        self.communicator.inform_verbose('Comparing...')

        duplicate_files = set()
        for i, p1 in enumerate(tqdm(self.files, desc='Total Progress',
                                    ascii=True, dynamic_ncols=True,
                                    leave=False)):

            if p1 not in self.processed_files and p1 not in self.duplicates:
                try:
                    # Read in the first photo
                    p1_value = Image.open(p1)
                    p1_hash = hash(p1_value.tobytes())

                    for j, p2 in enumerate(tqdm(self.files,
                                                desc='Current Image Progress',
                                                ascii=True, dynamic_ncols=True,
                                                leave=False)):

                        if p1 != p2 and p2 not in self.duplicates and \
                                p2 not in duplicate_files:

                            # Check the size of both files.
                            if p_file_sizes[i] == p_file_sizes[j]:

                                # Read in the second photo
                                p2_value = Image.open(p2)
                                p2_hash = hash(p2_value.tobytes())

                                # Check if their dimensions matches
                                if (p1_value.size == p2_value.size and
                                        p1_hash == p2_hash):

                                    self.add_to_duplicates(p1, p2)

                                    duplicate_files.add(p1)
                                    duplicate_files.add(p2)

                    self.processed_files.append(p1)

                    # Update the counter to make sure we have a backup.
                    if i % 10 == 0:
                        self.communicator.inform_debug('Backup Written. '
                                                       'Counter: {}'
                                                       .format(i),
                                                       getframeinfo(
                                                           currentframe()))
                        self.write_to_backup(self.duplicates,
                                             self.processed_files)
                except OSError:
                    self.communicator.inform_debug(
                        'An image could not be processed.',
                        getframeinfo(currentframe()))

        # Write one final time to the backup file.
        self.write_to_backup(self.duplicates, self.processed_files)
        self.report_service_time()
        return self.duplicates

    ############################################################################
    #                       COMPARE PHOTOS WITH CONFIDENCE                     #
    ############################################################################
    def compare_photos_with_confidence(self, confidence):
        """
        Find photos that are alike with a given confidence percentage.

        Algorithm:
        --> Fetch the file size
        --> Fetch the histogram and image dimensions
        --> Image comparing to others = p1, Other images = p2
        --> If p1 and p2 not yet processed and not yet in the duplicate set,
            I) Compare the file size
            II) Compare dimensions
            III) Check the histograms for intersection percentage and compare to
                 given confidence
        --> If these checks are all passed, the image is seen as duplicate of p1

        Backup's are written every processed p1 image due to
        more work per image.

        :param confidence: The percentage of certainty the images are alike.
        :return: All images matching the confidence criteria.
        """
        self.communicator.inform_verbose('Comparing...')

        duplicate_files = set()
        image_data = {}
        for i, p1 in enumerate(tqdm(self.files, desc='Total Progress',
                                    ascii=True, dynamic_ncols=True,
                                    leave=False)):

            self.communicator.inform_debug('Checking {}'.format(p1),
                                           getframeinfo(currentframe()))

            if p1 not in self.processed_files and p1 not in self.duplicates:
                try:
                    for j, p2 in enumerate(tqdm(self.files,
                                                desc='Current Image Progress',
                                                ascii=True, dynamic_ncols=True,
                                                leave=False)):

                        if p1 != p2 and p2 not in self.duplicates and \
                                p2 not in duplicate_files:

                            if p1 not in image_data:
                                image_data[p1] = self.get_image_data(p1)

                            if p2 not in image_data:
                                image_data[p2] = self.get_image_data(p2)

                            # I)
                            size_check = self.compare_file_size(
                                image_data.get(p1).get("size_on_disk"),
                                image_data.get(p2).get("size_on_disk"),
                                100000)

                            # II)
                            dim_check = self.compare_values(
                                image_data.get(p1).get("dimensions"),
                                image_data.get(p2).get("dimensions"))

                            # III)
                            hist_check = self.check_intersection(
                                self.return_intersection(
                                    image_data.get(p1).get("histogram"),
                                    image_data.get(p2).get("histogram")),
                                confidence)

                            if size_check and dim_check and hist_check:
                                self.communicator.inform_debug(
                                    'PASSED: Intersection test - {}'
                                    .format(p2),
                                    getframeinfo(currentframe()))

                                self.add_to_duplicates(p1, p2)

                                duplicate_files.add(p1)
                                duplicate_files.add(p2)

                    self.processed_files.append(p1)
                    self.communicator.inform_debug('Backup Written. '
                                                   'Counter: {}'
                                                   .format(i),
                                                   getframeinfo(
                                                       currentframe()))
                    self.write_to_backup(self.duplicates,
                                         self.processed_files)
                except OSError:
                    self.communicator.inform_debug(
                        'An image could not be processed.',
                        getframeinfo(currentframe()))

        self.communicator.inform_debug('The following dictionary is generated:',
                                       getframeinfo(currentframe()))
        self.communicator.inform_debug(str(self.duplicates),
                                       getframeinfo(currentframe()))

        # Write one final time to the backup file.
        self.write_to_backup(self.duplicates, self.processed_files)
        self.report_service_time()
        return self.duplicates

    ############################################################################
    #                                HELPER FUNCTIONS                          #
    ############################################################################
    def add_to_duplicates(self, p1, p2):
        """
        Add p2 to the duplicates. If p1 is not yet a key in duplicates, create
        this key. If p1 already exists, add p2 to its duplicates.
        :param p1: The key, used in duplicates
        :param p2: The value, appended to the key in duplicates.
        :return:
        """
        if p1 in self.duplicates:
            self.duplicates[p1].append(p2)
        else:
            self.duplicates[p1] = [p2]

    def write_to_backup(self, duplicates, processed_files):
        """
        Write current progress to a backup file.
        :param duplicates: The filenames of the currently found duplicates
        :param processed_files: The already processed filenames
        :return:
        """
        return self.backuphandler.write(duplicates, processed_files)

    def get_all_file_sizes(self):
        """
        Fetch all the file sizes on disk of the given files on init.
        :return: Array of file sizes.
        """
        p_file_sizes = []
        for p in tqdm(self.files, desc='Fetching file sizes', ascii=True,
                      dynamic_ncols=True, leave=False):
            p_file_sizes.append(self.get_file_size(p))
        return p_file_sizes

    def get_image_data(self, path_to_image):
        """
        Fetch the histogram, dimensions and size from the image and store them
        in a dictionary.

        :param path_to_image: The path to the image on disk.
        :return: Dict with histogram, dimensions and size as key
        """
        n_bins = 169
        try:
            p_value = Image.open(path_to_image)
            p_value_array = np.array(p_value)
            p_hist, _ = np.histogram(p_value_array, bins=n_bins)
            p_size = self.get_file_size(path_to_image)
            return {"histogram": p_hist,
                    "dimensions": p_value.size,
                    "size_on_disk": p_size}
        except TypeError:
            return {"histogram": np.zeros(n_bins),
                    "dimensions": (0, 0),
                    "size_on_disk": 0}
        except FileNotFoundError:
            return {"histogram": np.zeros(n_bins),
                    "dimensions": (0, 0),
                    "size_on_disk": 0}

    def update_service_time(self):
        """
        Update the time the comparator is currently serving.
        :return:
        """
        self.service_time = time() - self.service_time

    def report_service_time(self):
        """
        Report the time the comparator is currently serving.
        :return:
        """
        self.update_service_time()
        self.communicator.inform_verbose(
            'Comparator took {} to find {} files with duplicates.'
            .format(strftime('%Hh%Mm%Ss', gmtime(self.service_time)),
                    len(self.duplicates)))

    @staticmethod
    def get_file_size(file):
        """
        Fetch the file size on disk of the given file.
        :param file: The path to the file on disk
        :return: The size of the file in bytes.
        """
        try:
            return os.path.getsize(file)
        except OSError:
            return -1

    @staticmethod
    def return_intersection(hist_1, hist_2):
        """
        Calculate the intersection between two given histograms.
        :param hist_1: First histogram
        :param hist_2: Second histogram
        :return: The percentage of intersection as float.
        """
        minimum = np.minimum(hist_1, hist_2)
        intersection = np.true_divide(np.sum(minimum), np.sum(hist_2))
        return intersection

    @staticmethod
    def compare_file_size(image_1_size, image_2_size, marge):
        """
        Compare two file sizes within a certain marge.
        :param image_1_size: Image 1
        :param image_2_size: Image 2
        :param marge: Maximum allowed difference in bytes between two sizes.
        :return: True if file sizes are alike inside the marge.
        """
        return image_1_size - marge <= image_2_size <= image_1_size + marge

    @staticmethod
    def compare_values(value_1, value_2):
        return value_1 == value_2

    @staticmethod
    def check_intersection(intersection, confidence):
        return intersection > confidence * 0.01

    ############################################################################
    #                              DEPRECATED FUNCTIONS                        #
    ############################################################################
    def get_all_file_histograms_and_dimensions(self):
        """
        Fetch all the histograms and dimensions of the given files on init.

        :return: Array of histograms, Array of dimensions
        """
        p_file_histograms = []
        p_file_dimensions = []
        for p in tqdm(self.files, desc='Fetching histograms and dimensions',
                      ascii=True, dynamic_ncols=True, leave=False):
            hist, dim = self.get_image_data(p)
            p_file_histograms.append(hist)
            p_file_dimensions.append(dim)

        return p_file_histograms, p_file_dimensions

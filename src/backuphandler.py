# MIT License
#
# Copyright (c) 2018 Michiels Kilian
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# The BackupHandler handles all the reading and writing of backup files in the
# program.

import ast
import os
from inspect import currentframe, getframeinfo


class BackupHandler:
    temp_filename = "search_"
    backup_extension = ".backup"

    def __init__(self, communicator, filetype):
        self.communicator = communicator
        self.filetype = filetype
        self.communicator.inform_debug('Backup Handler created',
                                       getframeinfo(currentframe()))

    def read(self):
        try:
            with open(
                    self.temp_filename + self.filetype + self.backup_extension,
                    'r') as f:
                duplicates = ast.literal_eval(f.readline())
                processed_files = ast.literal_eval(f.readline())
                self.communicator.inform_debug('Dictionary created:',
                                               getframeinfo(currentframe()))
                self.communicator.inform_debug(str(duplicates),
                                               getframeinfo(currentframe()))
                return True, duplicates, processed_files
        except FileNotFoundError:
            self.communicator.inform_verbose('No backup found.')
            return False, None, None

    def write(self, duplicates, processed_files):
        try:
            f = open(self.temp_filename + self.filetype + self.backup_extension,
                     "w+")
            f.write(str(duplicates))
            f.write('\n')
            f.write(str(processed_files))
            f.close()
            return True
        except FileNotFoundError:
            self.communicator.inform_error('No backup could be written.')
            return False

    def remove_backup(self):
        try:
            os.remove(
                self.temp_filename + self.filetype + self.backup_extension)
        except FileNotFoundError:
            pass
        self.communicator.inform_success('Backup deleted.')

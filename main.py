# MIT License
#
# Copyright (c) 2018 Michiels Kilian
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# The main program which loads the different modules and orchestrates the flow.

import argparse
import sys
from inspect import currentframe, getframeinfo

if __name__ == "__main__":
    # Create parser
    argument_parser = argparse.ArgumentParser(
        description='Short program to delete the duplicate images on your '
                    'computer, given a certain directory and image file type.')

    # Required arguments
    required = argument_parser.add_argument_group('required arguments')
    required.add_argument('-l', '--location',
                          help='The location where to start the search. '
                               'The search will also look in all '
                               'subdirectories.', required=True)

    required.add_argument('-f', '--filetype',
                          help='The type of file you want to look for.',
                          required=True,
                          choices=['jpg', 'png', 'bmp', 'gif', 'tiff', 'ppm',
                                   'eps', 'icns', 'ico', 'im', 'msp', 'pcx',
                                   'sgi', 'tga', 'webp', 'xbm', 'psd', 'xpm'])

    # Optional arguments
    argument_parser.add_argument('-v', '--verbose',
                                 help='Show more information about what is '
                                      'happening.',
                                 action="store_true", required=False)

    argument_parser.add_argument('-d', '--debug',
                                 help='Debugging information for developers.',
                                 action="store_true", required=False)

    argument_parser.add_argument('-c', '--confidence',
                                 help='The confidence percentage (0 --> 100) '
                                      'you want the photos to be alike '
                                      '(integer).',
                                 type=int, required=False)

    # Read arguments
    args = vars(argument_parser.parse_args(sys.argv[2:]))

    # Create the communicator
    from src.communicator import Communicator

    communicator = Communicator(args['verbose'], args['debug'])

    # Check for confidence and if it is between 0 and 100
    if args['confidence'] is not None:
        if 0 > args['confidence'] or args['confidence'] > 100:
            communicator.inform_error('Please give a confidence '
                                      'between 0 and 100.')
            sys.exit(1)

    # Create the Backup Handler
    from src.backuphandler import BackupHandler

    backuphandler = BackupHandler(communicator, args['filetype'])
    # Start the search for backup
    backup_found, backup_files, processed_files = backuphandler.read()

    # Set up the program parameters.
    use_backup = False  # True if the user wants to use the backup.
    continue_search = True  # True if user wants to continue with the search.
    program_finished = False  # True if program finished without exceptions.

    # Request if user wants to continue with backup
    if backup_found:
        communicator.inform_debug('Backup was found. Length of the dictionary: '
                                  '{}'.format(len(backup_files.keys())),
                                  getframeinfo(currentframe()))

        answer = communicator.request_input(
            'Backup found of some previous .{} search.'
            '\n\nWhat would you like to do?'
            '\n1) Continue searching with this backup and add '
            'newly found duplicates to it.'
            '\n2) Start the cleaner and select which '
            'duplicates you want to delete from the backup.'
            '\n3) Don\'t use the backup and start a new search.'
            '\n\nPlease choose 1, 2 or 3: '.format(args["filetype"]),
            ['1', '2', '3'])

        if answer == '1':
            communicator.inform_verbose('Using backup.')
            use_backup = True
            continue_search = True
        elif answer == '2':
            communicator.inform_verbose('Using backup.')
            use_backup = True
            continue_search = False
        else:
            use_backup = False
            continue_search = True
            backup_files = None
            communicator.inform_verbose('Searching for new duplicates and '
                                        'adding them to the backup.')

    # Start the actual program.
    from src.file_organizer import FileOrganizer

    organizer = FileOrganizer(args['location'], args['filetype'],
                              communicator, backuphandler)

    files = None
    comparator = None
    cleaner = None
    duplicates = None

    ############################################################################
    #                                   SEARCH                                 #
    ############################################################################
    # Continue the search and add new found duplicates to the backup.
    if continue_search:
        files = organizer.find()

        if len(files) > 0:
            communicator.inform_verbose(
                'Found {} {} files.'.format(len(files),
                                            args['filetype']))

            from src.comparator import Comparator

            comparator = Comparator(files, communicator, backuphandler,
                                    backup_files, processed_files)

            if args['confidence'] is not None:
                duplicates = comparator.compare_photos_with_confidence(
                    args['confidence'])
            else:
                duplicates = comparator.compare_photos()

        else:
            communicator.inform_error(
                'No {} files found.'.format(args['filetype']))
            sys.exit(0)

    # Just use the already found backup files and start the cleaner.
    else:
        duplicates = backup_files

    ############################################################################
    #                                    CLEAN                                 #
    ############################################################################
    if len(duplicates) > 0:
        print('')  # For cleaner display in terminal

        from src.cleaner import Cleaner

        cleaner = Cleaner(duplicates, communicator)

        images_to_delete = cleaner.clean()

        if len(images_to_delete) > 0:
            communicator.inform_verbose('Files selected for deletion:')
            communicator.inform_list(images_to_delete)

            success, failed_files = organizer.delete(images_to_delete)
            if success:
                communicator.inform_success('All files should '
                                            'be deleted!')

            elif failed_files is not None:
                communicator.inform_error('It is possible that not all '
                                          'files could be deleted.')
                communicator.inform_list(failed_files)
                communicator.inform_verbose('Please check if the '
                                            'above files are '
                                            'actually deleted.')
        else:
            communicator.inform_warning('No files to delete.')
    else:
        communicator.inform_warning('No duplicates found.')

    # Clean up if requested AND program finished without errors.
    answer = communicator.request_input('Would you like to save the backup '
                                        'of this search? (y/N) ',
                                        ['y', 'Y', 'n', 'N'])
    if answer == 'n' or answer == 'N':
        backuphandler.remove_backup()
